class LinksController < ApplicationController
  before_action :verify_logged_in
  before_action :get_all_subs, except: [:destroy]
  before_action :find_link, only: [:edit, :update, :destroy]

  def index
    redirect_to sub_url(params[:sub_id])
  end

  def show
    @link = Link.find(params[:id])
    @comment = Comment.new
    render :show
  end

  def new
    @link = Link.new
    @link_subs = []
    render :new
  end

  def create
    @link = current_user.links.new(link_params)
    @link_subs = @link.subs.includes(:moderator)

    if @link.save
      flash[:notice] = "Thanks for your submission!"
      redirect_to sub_url(params[:sub_id])
    else
      flash[:errors] = @link.errors.full_messages
      render :new
    end
  end

  def edit
    render :edit
  end

  def update
    if @link.update_attributes(link_params)
      flash[:notice] = "Link updated!"
      redirect_to sub_url(params[:sub_id])
    else
      flash[:errors] = @link.errors.full_messages
      render :edit
    end
  end

  def destroy
    @link.destroy!
    redirect_to sub_url(params[:sub_id])
  end

  private
  def get_all_subs
    @all_subs = Sub.all
  end

  def link_params
    params.require(:link).permit(:title, :url, :sub_ids => [])
  end

  def find_link
    @link = current_user.links.includes(:subs).find(params[:id])
    @link_subs = @link.subs
  end
end
