class SubsController < ApplicationController
  before_action :verify_logged_in, except: [:index, :show]
  before_action :find_sub, only: [:show, :edit, :update, :destroy]
  before_action :verify_moderator, only: [:edit, :update, :destroy]

  def index
    @subs = Sub.all
    render :index
  end

  def show
    render :show
  end

  def new
    @sub = Sub.new
    5.times { @sub.links.new }
    @links = @sub.links
    render :new
  end

  def create
    @sub = current_user.moderated_subs.new(sub_params)
    @sub.links.new(links_params)

    @links = @sub.includes(:links)
    if @sub.save
      flash[:notice] = "#{ @sub.name } created successfully!"
      redirect_to sub_url(@sub)
    else
      (5 - links_params.count).times { @sub.links.new }
      flash[:errors] = @sub.errors.full_messages
      render :new
    end
  end

  def edit
    render :edit
  end

  def update
    if @sub.update_attributes(sub_params)
      flash[:notice] = "#{ @sub.name } updated successfully!"
      redirect_to sub_url(@sub)
    else
      flash[:errors] = @sub.errors.full_messages
      render :new
    end
  end

  def destroy
    @sub.destroy!
    redirect_to subs_url
  end

  private
  def sub_params
    params.require(:sub).permit(:name)
  end

  def links_params
    params.permit(:links => [:title, :url]).require(:links).values
          .reject{ |data| data.values.all?(&:blank?) }
          .map { |data| data[:user_id] = current_user.id; data }
  end

  def find_sub
    @sub = Sub.find(params[:id])
  end

  def verify_moderator
    redirect_to subs_url unless @sub.mod_id == current_user.id
  end
end
