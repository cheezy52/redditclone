class UserVotesController < ApplicationController
  def create
    vote = current_user.votes.create(vote_params)
    redirect_to link_url(vote.link)
  end

  private
  def vote_params
    params.require(:vote).permit(:link_id, :up)
  end
end
