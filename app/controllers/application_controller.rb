class ApplicationController < ActionController::Base
  include ApplicationHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def login!(user)
    user.generate_session_token!
    user.save!
    session[:session_token] = user.session_token
  end

  def logout!
    current_user.generate_session_token!
    current_user.save!
    session[:session_token] = nil
  end

  def verify_logged_in
    redirect_to new_session_url unless logged_in?
  end

  def logged_in?
    !!current_user
  end
end
