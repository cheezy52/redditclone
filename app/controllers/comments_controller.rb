class CommentsController < ApplicationController
  def new
    @comment = Comment.new
    render :new
  end

  def create
    @link = Link.find(params[:comment][:link_id])
    @comment = @link.comments.new(comment_params)
    flash[:errors] = @comment.errors.full_messages unless @comment.save
    redirect_to link_url(@link)
  end

  def show
    @existing_comment = Comment.find(params[:id])
    @comment = Comment.new
    render :show
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy!
    redirect_to link_url(@comment.link)
  end

  private
  def comment_params
    comment_params = params.require(:comment)
                           .permit(:body, :link_id, :parent_comment_id)
    comment_params[:user_id] = current_user.id
    comment_params
  end
end
