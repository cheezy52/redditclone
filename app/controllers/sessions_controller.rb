class SessionsController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.find_by_credentials(auth_params)

    if @user
      login!(@user)
      redirect_to root_url
    else
      flash.now[:errors] = ["Login failed"]
      render :new
    end
  end

  def destroy
    logout!
    redirect_to root_url
  end

  private
  def auth_params
    params.require(:user).permit(:username, :password)
  end
end
