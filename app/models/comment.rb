# == Schema Information
#
# Table name: comments
#
#  id                :integer          not null, primary key
#  body              :text
#  user_id           :integer
#  parent_comment_id :integer
#  link_id           :integer
#  created_at        :datetime
#  updated_at        :datetime
#

class Comment < ActiveRecord::Base
  validates :body, :presence => true

  has_many :comments, :inverse_of => :parent_comment,
    :foreign_key => :parent_comment_id

  belongs_to :parent_comment, :inverse_of => :comments,
    :class_name => "Comment"

  belongs_to :link
  belongs_to :user

  def comments_by_parent_id
    top_level_comments = self.comments
    comment_chains = {}
    top_level_comments.map do |comment|
      comment_chains[comment.id] = comment.comments
    end
    comment_chains
  end
end
