# == Schema Information
#
# Table name: subs
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  mod_id     :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Sub < ActiveRecord::Base
  validates :name, :mod_id, :presence => true

  belongs_to :moderator, :inverse_of => :moderated_subs,
  :foreign_key => :mod_id,
  :class_name => "User"

  has_many :sub_links, :inverse_of => :sub
  has_many :links, :through => :sub_links
end
