class UserVote < ActiveRecord::Base
  validates :link_id, :uniqueness => { scope: :user_id }

  belongs_to :user
  belongs_to :link
end
