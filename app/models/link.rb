# == Schema Information
#
# Table name: links
#
#  id         :integer          not null, primary key
#  title      :string(255)      not null
#  url        :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Link < ActiveRecord::Base
  validates :title, :url, :user, :presence => true

  belongs_to :user, :inverse_of => :links
  has_many :sub_links, :inverse_of => :link
  has_many :subs, :through => :sub_links
  has_many :comments, :inverse_of => :link
  has_many :votes, :class_name => "UserVote"

  def comments_by_parent_id
    top_level_comments = self.comments
    comment_chains = {}
    top_level_comments.map do |comment|
      comment_chains[comment.id] = comment.comments
    end
    comment_chains
  end
end
