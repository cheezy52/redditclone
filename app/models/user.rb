# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  validates :username, :password_digest, :session_token, :presence => true
  validates :username, :session_token, :uniqueness => true
  validates :password, :length => {:minimum => 4, allow_nil: true}
  before_validation :ensure_session_token
  has_many :votes, :class_name => "UserVote"

  has_many :moderated_subs,
    :foreign_key => :mod_id,
    :class_name => "Sub"
  has_many :links, :inverse_of => :user

  def password
    @password
  end

  def self.find_by_credentials(params)
    user = User.find_by_username(params[:username])
    return user if user && user.is_password?(params[:password])
    nil
  end

  def generate_session_token!
    self.session_token = SecureRandom::urlsafe_base64(16)
  end

  def ensure_session_token
    self.session_token ||= SecureRandom::urlsafe_base64(16)
  end

  def password=(plaintext)
    unless plaintext.nil?
      @password = plaintext
      self.password_digest = BCrypt::Password.create(plaintext)
    end
  end

  def is_password?(plaintext)
    BCrypt::Password.new(self.password_digest).is_password?(plaintext)
  end
end
