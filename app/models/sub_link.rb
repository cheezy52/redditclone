# == Schema Information
#
# Table name: sub_links
#
#  id         :integer          not null, primary key
#  sub_id     :integer          not null
#  link_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class SubLink < ActiveRecord::Base
  validates :sub, :link, :presence => true

  belongs_to :sub, :inverse_of => :sub_links
  belongs_to :link, :inverse_of => :sub_links
end
