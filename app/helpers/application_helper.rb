module ApplicationHelper
  def csrf_token
    "<input type=\"hidden\" name=\"authenticity_token\" value=\"#{form_authenticity_token}\">".html_safe
  end

  def patch_method
    if params[:action] == "edit" || params[:action] == "update"
      "<input type=\"hidden\" name=\"_method\" value=\"PATCH\">".html_safe
    end
  end

  def current_user
    @current_user ||= User.find_by_session_token(session[:session_token])
  end
end
