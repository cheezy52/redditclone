RedditClone::Application.routes.draw do
  root to: "sessions#new"
  resources :users
  resource :session
  resources :subs
  resources :links do
    post 'upvote', to: "user_votes#create"
    post 'downvote', to: "user_votes#create"
  end
  resources :comments
end
