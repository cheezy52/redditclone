# == Schema Information
#
# Table name: links
#
#  id         :integer          not null, primary key
#  title      :string(255)      not null
#  url        :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Link do
  it { should belong_to :user }
  it { should have_many :subs }

  it "should be invalid without a title" do
    test_link = FactoryGirl.build(:link, :title => nil)
    expect(test_link).not_to be_valid
  end

  it "should be invalid without a url" do
    test_link = FactoryGirl.build(:link, :url => nil)
    expect(test_link).not_to be_valid
  end

  it "should be invalid without a user_id" do
    test_link = FactoryGirl.build(:link, :user_id => nil)
    expect(test_link).not_to be_valid
  end

  it "should be valid with all attributes set" do
    test_link = FactoryGirl.create(:link)
    expect(test_link).to be_valid
  end

end
