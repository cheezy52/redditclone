# == Schema Information
#
# Table name: comments
#
#  id                :integer          not null, primary key
#  body              :text
#  user_id           :integer
#  parent_comment_id :integer
#  link_id           :integer
#  created_at        :datetime
#  updated_at        :datetime
#

require 'spec_helper'

describe Comment do
  it {should have_many :child_comments}
  it {should belong_to :parent_comment}
  it {should belong_to :user}
  it {should belong_to :link}

  it "can have child comments" do
    test_comment = FactoryGirl.create(:comment)
    test_child = FactoryGirl.create(:comment, :parent_comment_id => test_comment.id)
    expect(test_comment.child_comments).to include(test_child)
  end

  it "can have a parent comment" do
    test_comment = FactoryGirl.create(:comment)
    test_child = FactoryGirl.create(:comment, :parent_comment_id => test_comment.id)
    expect(test_child.parent_comment).to eq(test_comment)
  end

  it "does not have parent comment when top-level" do
    test_comment = FactoryGirl.create(:comment)
    expect(test_comment.parent_comment).to be_nil
  end
end
