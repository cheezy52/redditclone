# == Schema Information
#
# Table name: subs
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  mod_id     :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Sub do
  it { should have_many :links }
  it { should belong_to :moderator }

  it "is invalid without a name" do
    test_sub = FactoryGirl.build(:sub, :name => nil)
    expect(test_sub).not_to be_valid
  end

  it "is invalid without a moderator" do
    test_sub = FactoryGirl.build(:sub, :mod_id => nil)
    expect(test_sub).not_to be_valid
  end

  it "is valid with a name and moderator" do
    test_sub = FactoryGirl.build(:sub)
    expect(test_sub).to be_valid
  end
end
