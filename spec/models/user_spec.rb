# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

require 'spec_helper'

describe User do

  it "is not valid without a username" do
    testuser = FactoryGirl.build(:user, :username => nil)
    expect(testuser).not_to be_valid
  end

  it "is not valid without a password" do
    testuser = FactoryGirl.build(:user, :password => nil)
    expect(testuser).not_to be_valid
  end

  it "has a session token on save" do
    testuser = FactoryGirl.create(:user)
    expect(testuser.session_token).not_to be_nil
  end

  it "is valid with username and password" do
    testuser = FactoryGirl.build(:user)
    expect(testuser).to be_valid
  end

  it "finds user by credentials" do
    test_username = Faker::Internet.user_name
    test_password = Faker::Internet.password
    testuser = User.create(:username => test_username, :password => test_password)
    found_user = User.find_by_credentials({:username => test_username, :password => test_password})
    expect(found_user).to eq(testuser)
  end

  it { should have_many :links }
  it { should have_many :moderated_subs }
end
