# == Schema Information
#
# Table name: comments
#
#  id                :integer          not null, primary key
#  body              :text
#  user_id           :integer
#  parent_comment_id :integer
#  link_id           :integer
#  created_at        :datetime
#  updated_at        :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    body Faker::Lorem.sentence
    user_id do
      FactoryGirl.create(:user).id
    end
    parent_comment_id nil
    link_id do
      FactoryGirl.create(:link).id
    end
  end
end
